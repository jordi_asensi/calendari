-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: app
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `userid` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cat_userid` (`userid`),
  CONSTRAINT `fk_cat_userid` FOREIGN KEY (`userid`) REFERENCES `usuari` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'METGE PAU',7),(2,'METGE ROGER',7),(3,'METGE JORDI',7),(4,'METGE SUSANA',7),(5,'DENTISTA PAU',7),(6,'DENTISTA ROGER',7),(7,'DENTISTA JORDI',7),(8,'DENTISTA SUSANA',7),(9,'ALTRES',7),(11,'REUNIO INSTITUT',NULL),(12,'Enjoy Reading',NULL);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `event` varchar(100) DEFAULT NULL,
  `data_inici` datetime DEFAULT NULL,
  `data_fi` datetime DEFAULT NULL,
  `categoriaid` mediumint(8) unsigned DEFAULT NULL,
  `descripcio` varchar(400) DEFAULT NULL,
  `creat` datetime DEFAULT NULL,
  `usuariId` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_event_categoriaid` (`categoriaid`),
  KEY `FK_EVENT_USER` (`usuariId`),
  CONSTRAINT `fk_event_categoriaid` FOREIGN KEY (`categoriaid`) REFERENCES `categoria` (`id`),
  CONSTRAINT `FK_EVENT_USER` FOREIGN KEY (`usuariId`) REFERENCES `usuari` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (8,'Reunió cole','2015-10-20 00:00:00',NULL,NULL,'Reunió pau','2015-10-20 18:05:07',NULL),(10,'Biblioteca','2015-10-31 12:00:00',NULL,NULL,'He d\'acabar el llibre','2015-10-21 19:50:57',NULL),(22,'jjj','2015-10-11 00:00:00',NULL,NULL,'jjj','2015-10-30 09:13:53',NULL),(23,'1234','2015-10-01 23:00:00',NULL,NULL,NULL,'2015-10-30 09:21:05',NULL),(24,'kjjjjjj','2015-10-31 00:00:00',NULL,NULL,'jjj','2015-10-30 14:09:00',NULL),(25,'ara si, no?','2015-10-31 00:00:00',NULL,NULL,'ara a veure si funciona','2015-10-30 14:16:46',NULL),(26,'Data','2015-10-31 00:00:00',NULL,NULL,'data','2015-10-30 14:22:53',NULL),(27,'gdfgdfg','2015-11-01 00:00:00',NULL,NULL,NULL,'2015-10-30 14:27:39',NULL),(28,'mmmmmmmm','2015-10-31 00:00:00',NULL,NULL,NULL,'2015-10-30 14:31:14',NULL),(29,'aaaa','2015-10-31 00:00:00',NULL,NULL,NULL,'2015-10-30 22:26:19',NULL),(30,'hjhhk','2015-10-31 00:00:00',NULL,NULL,NULL,'2015-10-30 22:28:22',NULL),(31,'kkkkk','2015-10-31 00:00:00',NULL,NULL,NULL,'2015-10-30 22:29:26',NULL),(32,'AAAAA','2015-10-31 00:00:00',NULL,NULL,NULL,'2015-10-30 22:30:40',7),(33,'rvrr','2015-11-01 00:00:00',NULL,NULL,NULL,'2015-11-01 17:42:45',7),(34,NULL,'2015-11-05 12:12:00',NULL,2,NULL,'2015-11-03 18:24:07',7),(35,'PROVA','2015-11-20 10:00:00',NULL,NULL,'descripció de prova','2015-11-03 19:09:18',7);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuari`
--

DROP TABLE IF EXISTS `usuari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuari` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuari`
--

LOCK TABLES `usuari` WRITE;
/*!40000 ALTER TABLE `usuari` DISABLE KEYS */;
INSERT INTO `usuari` VALUES (7,'123','oo@oo','$2y$11$ff9537f0cb0672becd3abutTFxONEr7sbB6/pgyOm3jDddR7/kU3G');
/*!40000 ALTER TABLE `usuari` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-11 21:40:43
