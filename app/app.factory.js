( function() { 
  function UserFactory($cookies,$q,asyncService) {
      var info = {
        userName : '',
        logged : false,
        sid : ''
      };
      function reset() {
					$cookies.remove("cal.user");
					$cookies.remove("cal.sid");
					$cookies.remove("cal.logged");
					info.userName='';
					info.logged=false;
					info.sid='';
      }
      function isLogged() {
          return info.logged;
      };
      function setLogginName(user) {
          info.userName = user;
          info.logged = true;
      };
      function setSid(sid) {
          info.sid=sid;
      }
      function setUserAndSid(user,sid) {
          info.userName=user;
          info.sid=sid;
          info.logged = true;
      }
      function getDataFromCookies() {
        var data={
          user : $cookies.get("cal.user"),
          sid : $cookies.get("cal.sid"),
          logged : $cookies.get("cal.logged")
        }
        return data;
      }
      function setDataFromCookies()
      {
        info.userName=$cookies.get("cal.user");
        info.sid=$cookies.get("cal.sid");
        info.logged=$cookies.get("cal.logged");
      }
      function setToCookie() {
					$cookies.put("cal.user",info.userName);
					$cookies.put("cal.sid",info.sid);
					$cookies.put("cal.logged",info.logged);

      }
      function getCookies(data) {
          data.user=$cookies.get("cal.user");
          data.sid=$cookies.get("cal.sid");
          data.logged=$cookies.get("cal.logged");
          info.userName=data.user;
          info.sid=data.sid;
          info.logged=data.logged;
      }
      function getUser()
      {
        return info.userName;
      }

	function logIn(user,password){
	    var data={ user:user, password:password };
			var p=asyncService.postData('/datos/logIn', data)
			.then (function(result){
			  console.log(result);
				if (result.data.success=='true')
				{
				  info.userName=user;
				  info.sid=result.data.sid;
				  info.logged=true;
          setToCookie();
					return result;
				}
				else
				{
				  console.log("rejected");
				  $q.reject(result);
				}
			}, function(error){
				  $q.reject(error);
			});
			return p;
	};
	
	function logOut() {
	  reset();
	  var p=asyncService.postData('/datos/logOut', {})
	  p.then(function(result) { console.log(result)},function(error) { console.log(error)});
	  return p;
	  
	}

	function checkSession()
	{
	  /*
		var data= {
		  user : info.user, sid :info.sid
		  }
		  
		if (!data.user) {
		  data=getDataFromCookies();
		}
		*/
		var  data=getDataFromCookies();

		var p=asyncService.postData("/datos/checkSession",data);
		p.then(
			function(result){
				if (result.data.success=="true") {
					console.log(result);
					setDataFromCookies();
				}
				else {
					$cookies.remove("cal.user");
					$cookies.remove("cal.sid");
					$cookies.remove("cal.logged");
				}
			}, function(result) {
			alert("error");
		});
		return p;
	}

  info.isLogged=isLogged;
  info.setUserAndSid=setUserAndSid;
  info.logIn=logIn;
  info.checkSession=checkSession;
  info.getUser=getUser;
  info.getDataFromCookies=getDataFromCookies;
  info.setDataFromCookies=setDataFromCookies;
  info.setToCookie=setToCookie;
  info.reset=reset;
  info.logOut=logOut;
  return info;
      
  };

  function ConfigFactory() {
      var config = {
        itemsPerPage : 3
      };
      function getItemsPerPage() {
          return config.itemsPerPage;
      };
      function setItemsPerPage(numItems) {
          config.itemsPerPage=numItems;
      };
  };

 UserFactory.$inject=["$cookies","$q","asyncService"];

 angular.module("calApp").factory('UserFactory',UserFactory);
 angular.module("calApp").factory('ConfigFactory',UserFactory);

})();

