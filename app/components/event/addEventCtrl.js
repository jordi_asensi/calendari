( function() {

	
function frm ($scope,$filter,$routeParams,$http,$cookies,$location, $modalInstance,UserFactory,asyncService) {

    function getCat(user,sid) {
        var data= { user: user , sid : sid };
        var p=asyncService.postData('/datos/getCategoria',data);
        return p;
    }

    function ok() {
       vm.evt.dateStr=$filter('date')(vm.evt.data,'yyyy-MM-dd');
       vm.evt.hourStr=$filter('date')(vm.evt.hour,'HH:mm');
       vm.evt.user=UserFactory.getUser();
        console.log(vm.evt);
       var p=asyncService.postData('/datos/insertEvent',vm.evt)
        .then( function(data) {
            console.log(data);
            alert("insertado ok");
           $modalInstance.close();
       }, function(error) {
            console.log(error);
            alert("insertado error");
           $modalInstance.close();
           $location.path("/auth/login");
        });

    }
    function addCat() {
        vm.evt.addNewCat=1;
    }
    
    function showNom() {
        vm.evt.showNom=1;
        console.log(vm.evt);
    }

	var vm=this;
	vm.evt={};
	vm.getCat=getCat;
	vm.addCat=addCat;
    vm.showNom=showNom;
    
	vm.ok=ok;
	var logged = $cookies.get("cal.logged");
	vm.user="";
	if (logged=="true")
	{
		var p=getCat($cookies.get("cal.user"),$cookies.get("cal.sid"));
		p.success(function (data) {
//		    console.log(data);
//		    data.categoria.unshift({Id:'0',Nom:'SENSE CATEGORIA',UserId:'0'});
		    vm.cat=data.categoria;
		    
		}).error(function (error) {
		    $location.path("/auth/login");
		})
	}
	else {
		$location.path("/auth/login");
	}
}

frm.$inject = ['$scope','$filter','$routeParams','$http','$cookies','$location','$modalInstance','UserFactory','asyncService'];

angular.module('calApp')
 .controller('addEventCtrl',frm);	

})();

