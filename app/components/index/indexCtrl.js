( function() {

	
function index ($scope,$rootScope,$routeParams,$http,$location,$cookies,asyncService,UserFactory) {


	function isLogged() {
		ixvm.user=UserFactory.getUser();
		return UserFactory.isLogged();
	}

	function getUser() {
		return UserFactory.getUser();
	}
	
	function logOut() {
		UserFactory.logOut().then(function() { $location.path("/auth/login")} );
	}
	var ixvm=this;
	ixvm.isLogged=isLogged;
	ixvm.logOut=logOut;


}

index.$inject = ['$scope','$rootScope','$routeParams','$http','$location','$cookies','asyncService','UserFactory'];

angular.module('calApp')
 .controller('indexCtrl',index);	

})();


