( function() {

	
function frm ($scope,$filter,$routeParams,$http,$cookies,$location, $uibModal,asyncService,UserFactory) {

	function showFilter()
	{
		vm.filter.show=vm.filter.show ? false : true; 
	}
	function showPrint()
	{
		vm.filter.isPrint=vm.filter.isPrint ? false : true; 
		getEvents(1);
	}
	function updateFilter()
	{
		if (vm.filter.enabled) getEvents(1);
	}
	function applyFilter()
	{
		vm.filter.enabled=vm.filter.enabled ? false : true; 
		getEvents(1);
	}
	function changeOrder()
	{
		vm.filter.order=(vm.filter.order=='asc') ? 'desc' : 'asc'; 
		getEvents(1);
	}
	function deleteEvent(events,index)
	{
		event=events[index];
		event["user"]=$cookies.get("cal.user");
		event["sid"]=$cookies.get("cal.sid");
		var p=asyncService.postData("/datos/delEvent",event);
		p.then(
			function(result){
				var data=result.data;
				if (data.success=="true") {
					console.log(data);
					vm.getEvents(vm.pag.currentPage);
				}
			},
			function(error) {
				
			}
			
			);
	}

	function getEvents(page)
	{
		var data=UserFactory.getDataFromCookies();
		data.page=page;
		data.eventList=vm.eventList;
		data.filter=vm.filter;
		data.itemsPerPage=vm.itemspp;
		if (data.filter.dateFrom) data.filter.dateFromStr=$filter('date')(vm.filter.dateFrom,'yyyy-MM-dd');
		if (data.filter.dateTo) data.filter.dateToStr=$filter('date')(vm.filter.dateTo,'yyyy-MM-dd');
		console.log(vm.filter);
		var p=asyncService.postData("/datos/getEvents",data);
		p.then(
			function(result){
				data=result.data;
				if (data.success=="true") {
					console.log(data);
					vm.events=data.events;
					vm.pag={};
					vm.pag.pages=data.pages;
					vm.pag.currentPage=data.currentPage;
					vm.pag.totalItems=data.totalItems;
					vm.pag.itemsPerPage=data.itemsPerPage;
				}
				else {
					alert("Error. Sesion caducada");
					$location.path("/auth/login");
				}
			},
			function(error) {
				
			}
			
			);
	}


	function checkSession()
	{
		alert("checksession");
		var p=UserFactory.checkSession();
		p.then(
			function(result) {
				if (result.data.success=="true") {
					console.log("checksession-1: " + result.data.user + "-" + result.data.sid);
					vm.user=result.data.user;
					vm.sid=result.data.sid;
					getEvents();
				}
				else {
						UserFactory.reset();
						$location.path("/auth/login");
				}
			
			},
			function(error) {
				
			});
	}
	
	function refresh() {
		vm.getEvents(1);
	}

	function pageChanged() {
		vm.getEvents(vm.pag.currentPage);
	}

	function addItem(event) {
       var p=$uibModal.open({
            templateUrl: '/app/components/event/addEvent.html',
            backdrop: true,
            windowClass: 'modal',
			controller: "addEventCtrl",
			controllerAs: "vm"
        });
        p.result.then(function() { vm.getEvents(vm.pag.currentPage)});


	}
	var vm=this;
	vm.addItem=addItem;
    vm.checkSession=checkSession;
    vm.getEvents=getEvents;
    vm.deleteEvent=deleteEvent;
    vm.pageChanged=pageChanged;
    vm.refresh=refresh;
    vm.showFilter=showFilter;
    vm.applyFilter=applyFilter;
    vm.updateFilter=updateFilter;
    vm.showPrint=showPrint;
    vm.changeOrder=changeOrder;
    vm.filter={};
	var logged = $cookies.get("cal.logged");
	UserFactory.getDataFromCookies();
	if (logged=="true")
	
	{
		var status=checkSession();
	}
	else {
		$location.path("/auth/login");
	}
	

	
}

frm.$inject = ['$scope','$filter','$routeParams','$http','$cookies','$location','$uibModal','asyncService','UserFactory'];

angular.module('calApp')
 .controller('mainCtrl',frm);	

})();

