( function() {
function service ($http,$q) {
      return {
        loadDataFromUrls: function(urls) {
          var deferred = $q.defer();
          var urlCalls = [];
          angular.forEach(urls, function(url) {
            urlCalls.push($http.get(url.url));
          });
          // they may, in fact, all be done, but this
          // executes the callbacks in then, once they are
          // completely finished.
          $q.all(urlCalls)
          .then(
            function(results) {
            deferred.resolve(results); 
          },
          function(errors) {
            deferred.reject(errors);
          },
          function(updates) {
            deferred.update(updates);
          });
          return deferred.promise;  
          },
        postData: function(url,data) {
            var d=$http({
              url: url,
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              data: data
            })
            return d;
        }
          
      };
}
service.$inject=['$http','$q'];
	
angular.module('calApp')
.service('asyncService', service);
})();

