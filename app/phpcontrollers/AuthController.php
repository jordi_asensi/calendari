<?php
class AuthController extends Controller
{
    
    function setData($dataIn)
    {
        $dataOut=[];
        $dataOut['status']='success';
        $dataOut['user']=$dataIn->user;
        session_start();
        session_regenerate_id(true); 
        $session=session_id();
        $_SESSION['user'] = $dataIn->user;
        $_SESSION['timeout'] = time();
        $dataOut['sid']=$session;
        $dataOut['success']='true';
        $dataOut['message']='user logged in';
        return $dataOut;
    }
    function generateHash($password) {
        if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
            $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
            return crypt($password, $salt);
        }
    }
    function verify($password, $hashedPassword) {
        return crypt($password, $hashedPassword) == $hashedPassword;
    }
   	public function signIn()
    {
        // id , equipoId, incidencia
        $postdata = file_get_contents("php://input");
        $signInData = json_decode($postdata);
        $user=UsuariQuery::create()->findOneByNom($signInData->user);
        if (!$user)
        {
            $user=new Usuari();
            $pass=$this->generateHash($signInData->password);
            $user->setNom($signInData->user);
            $user->setPassword($pass);
            $user->setMail($signInData->email);
            $user->save();
            echo json_encode($this->setData($user));
        }
    }
    
    public function logIn()
    {
        // id , equipoId, incidencia
        $postdata = file_get_contents("php://input");
        $logInData = json_decode($postdata);
        $data=[];
        $data['success']='error';
        $data['message']='Invalid username or password';
        $user=UsuariQuery::create()->filterByNom($logInData->user)->findOne();
        if (!$user)  {
        }
        else {
            $userpass=$user->getPassword();    
            if ($this->verify($logInData->password, $userpass)) {
                $data=$this->setData($logInData);
            }
        }
        echo json_encode($data);
    }

    public function logOut()
    {
        session_start();
        session_destroy();
        
    }
    
    public function checkSessionData($sessionData)
    {
        file_put_contents("checksessiondata.txt",var_export($sessionData,true));
        session_start();
        $data=[];
        $data["phpsid"]=session_id();
        $data["phpuser"]=$_SESSION["user"];
        $data["user"]=$sessionData->user;
        $data["sid"]=$sessionData->sid;
        $data["timeout"]=$_SESSION['timeout'];
        $data["time"]=time();
        $data["diff"]=$_SESSION['timeout'] + 60*10 - $data["time"];
        if (session_id()==$sessionData->sid && $_SESSION["user"]==$sessionData->user && $data["diff"]>0) {
            $data["success"]="true";
            $_SESSION['timeout'] = time();
        }
            
        else
            $data["success"]="false";
        file_put_contents("checksessiondata2.txt",var_export($data,true));
        return $data;        
    }
    
    public function checkSession() {
        $postdata = file_get_contents("php://input");
        $checkSessionData = json_decode($postdata);
        $data=$this->checkSessionData($checkSessionData);  
        echo json_encode($data);
        
    }
}