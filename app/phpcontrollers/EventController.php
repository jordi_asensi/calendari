<?php
class EventController extends Controller
{
    function insertEvent()
    {
         $con = \Propel\Runtime\Propel::getConnection(); $con->useDebug(true); 
        $postdata = file_get_contents("php://input");
        $getEvtData = json_decode($postdata);
        $auth  = new AuthController();
        $data = $auth->checkSessionData($getCatData);
        if ($data["status"]="success") {
            $event = new Event();
            $event->setEvent($getEvtData->nom);
            $event->setDataInici($getEvtData->dateStr . $getEvtData->hourStr . ":00");
            $event->setDescripcio($getEvtData->desc);
            $event->setCreat(date('Y-m-d H:i:s'));
            $userid=UsuariQuery::create()->findOneByNom($getEvtData->user);
            $event->setUsuariid($userid->getId());
            if (isset($getEvtData->addNewCat))
            {
                $cat=new CategoriaController();
                $catId=$cat->CreateCat($getEvtData->newcat,$getEvtData->cat->Userid);
                $event->setCategoriaId($catId);
            }
            else
            {
                $event->setCategoriaId($getEvtData->cat->Id);
            }
            $event->save();

            $data["catId"]=$event->Id;
            $date["inserted"]="true";
            
        }
        echo json_encode($data);
    }
    function applyFilter($query,$evtData) {
        if ($evtData->filter->enabled) {
            if (isset($evtData->filter->dateFromStr) || isset($evtData->filter->dateToStr)) {
                $evtData->eventList="All";
                file_put_contents("getEventsF0.txt",var_export($evtData->filter,true));
                if (isset($evtData->filter->dateFromStr) && isset($evtData->filter->dateToStr)) {
                    file_put_contents("getEventsF1.txt",var_export($evtData->filter,true));
                    $query=$query->condition('cond1a','Event.DataInici>=?',$evtData->filter->dateFromStr);
                    $query=$query->condition('cond1b','Event.DataInici<=?',$evtData->filter->dateToStr . ' 23:59');
                    $query=$query->combine(array('cond1a','cond1b'),'and','cond1');
                }
                else {
                    file_put_contents("getEventsF2.txt",var_export($evtData->filter,true));
    
                    if (isset($evtData->filter->dateFromStr)) {
                        file_put_contents("getEventsF3.txt",var_export($evtData->filter,true));
                        $query=$query->condition('cond1','Event.DataInici>=?',$evtData->filter->dateFromStr);
                    }
                    else {
                        file_put_contents("getEventsF4.txt",var_export($evtData->filter,true));
                        $query=$query->condition('cond1','Event.DataInici<=?',$evtData->filter->dateToStr . ' 23:59');
                    }
                }
            }
            else {
                file_put_contents("getEventsF5.txt",var_export($evtData->filter,true));
                $query=$query->condition('cond1','Event.DataInici is not null');
            }
            if (isset($evtData->filter->text)) {
                $query=$query->condition('cond2a','UPPER(Event.Event) LIKE ?','%' . $evtData->filter->text . '%',PDO::PARAM_STR);
                $query=$query->condition('cond2b','UPPER(Categoria.Nom) LIKE ?','%' . $evtData->filter->text . '%',PDO::PARAM_STR);
                $query=$query->combine(array('cond2a','cond2b'),'or','cond2');
            }
            else {
                $query=$query->condition('cond2','Event.Id is not null');
            }
            $query=$query->where(array('cond1','cond2'),'and');
            
        }

        if (isset($evtData->eventList)) {
            switch($evtData->eventList) {
                case "All":
                    break;
                case "Future":
                    $query=$query->where('Event.DataInici >= ?',date("Y-m-d"));
                    break;
                case "Past":
                    $query=$query->where('Event.DataInici < ?',date("Y-m-d"));
                    break;
            }
        }

    }
    function getEvents()
    {
        $con = \Propel\Runtime\Propel::getConnection(); $con->useDebug(true); 
        $postdata = file_get_contents("php://input");
        $getEvtData = json_decode($postdata);
        $auth  = new AuthController();
        file_put_contents("getEvents.txt",var_export($getEvtData,true));    
        $data = $auth->checkSessionData($getEvtData);
        $askPage=1;
        $itemsPerPage=5;
        $isPrint=false;
        if (isset($getEvtData->filter->isPrint)) $isPrint=$getEvtData->filter->isPrint;
        if ($data["status"]="success") {
            if (isset($getEvtData->page)) {
                $askPage=$getEvtData->page;
                $itemsPerPage=$getEvtData->itemsPerPage;
            }
            $cal_ini=EventQuery::create()->leftJoinWith('Categoria')->joinWith('Usuari')
                ->select(array('Id','Event','DataInici','Descripcio'))
                ->withColumn('Categoria.Nom','CategoriaNom')
                ->withColumn('Usuari.Nom','UsuariNom');
            $cal_ini=$cal_ini->where('Usuari.Nom=?',$getEvtData->user);
            
            // Filters
            //$cal_ini=$cal_ini->where('cond1');
            $this->applyFilter($cal_ini,$getEvtData);
            if (isset($getEvtData->filter->order)) { 
                $order=$getEvtData->filter->order;
                $cal_ini=$cal_ini
                    ->orderByDataInici($order);
            }
            else {
                $cal_ini=$cal_ini
                    ->orderByDataInici();
            }
                
            if (!$isPrint) {
                $cal_ini=$cal_ini->paginate($page = $askPage, $rowsPerPage = $itemsPerPage);
                $data["pages"]=$cal_ini->getLastPage();
                $data["totalItems"]=$cal_ini->getNbResults();
            }
            $cal=$cal_ini;
            
            $events=[];
            foreach ($cal as $event) {
                        $events[]=$event;
                    }
            $data["currentPage"]=$askPage;
            $data["itemsPerPage"]=$itemsPerPage;
            $data["sql"]=$con->getLastExecutedQuery(); 
            $data["events"]=$events;
        }
        echo json_encode($data);

    }
    function deleteEvent()
    {
        $postdata = file_get_contents("php://input");
        $getEvtData = json_decode($postdata);
        $auth  = new AuthController();
        $data = $auth->checkSessionData($getEvtData);
        if ($data["status"]="success") {
            $data["event"]=$getEvtData;
            $cat=EventQuery::create()->findPk($getEvtData->Id);
            $cat->delete();
        }
        echo json_encode($data);

    }

}