<?php
class CategoriaController extends Controller
{
    function getUserCat()
    {
        $postdata = file_get_contents("php://input");
        $getCatData = json_decode($postdata);
        $auth  = new AuthController();
        $data = $auth->checkSessionData($getCatData);
        file_put_contents("log_getuser.txt", var_export($data,true) . var_export ($getCatData,true));
        $user = UsuariQuery::create()->findOneByNom($getCatData->user);
        file_put_contents("log_getuser2.txt", var_export($cat,true));
        $cat = CategoriaQuery::create()->useUsuariQuery()->filterByNom($getCatData->user)->endUse()->find()->toArray();
        file_put_contents("log_getuser3.txt", var_export($cat,true));
        $data["categoria"]=$cat;
        echo json_encode($data);
    }
    function createCat($categoria, $userid)
    {
        $cat = new Categoria();
        $cat->setNom($categoria);
        $cat->setUserId($userid);
        $cat->save();
        file_put_contents("log_createCat2.txt", var_export($cat,true));
        return $cat->Id;
        
    }

}