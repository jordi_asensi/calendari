( function() { 
	var configRoute = function ($routeProvider,$locationProvider) {
	    $locationProvider.html5Mode({enabled:true,requireBase: false});        

		$routeProvider
		.when("/", {templateUrl: "app/components/main/main.html",
				   controller: "mainCtrl",
					controllerAs: "mainvm"
				   })
		.when("/index.html", {templateUrl: "app/components/main/main.html",
				   controller: "mainCtrl",
					controllerAs: "mainvm"
				   })
		.when("/auth/login", {templateUrl: "/app/components/auth/login.html",
				   controller: "logInCtrl",
					controllerAs: "livm"
				   })
		.when("/auth/signin", {templateUrl: "/app/components/auth/signIn.html",
				   controller: "signInCtrl", 
				   controllerAs: "sivm"
				   })
		.when("/404", {templateUrl: "app/components/404/404.html"})
		.otherwise({ redirectTo: "/404.html"});
		
	};
	configRoute.$inject=['$routeProvider','$locationProvider'];
	angular.module("calApp").config(configRoute);


})();

